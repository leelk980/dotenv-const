# Dotenv-Const

Convert dotenv file to typescript object. (for type-safe development)  
it supports basic typescript primitive types (details in example)

1. string
2. number
3. boolean

## Installation

```
npm i -D dotenv-const
```

## Run

```
npx dotenv-const -f <env file path> -o <output file path>
```

## Example

### Run command

```
npx dotenv-const -f env/.env.local -o env/environment.ts
```

### Target .env file

```dotenv
TZ=UTC
NODE_ENV=local

# DATABASE
DATABASE_HOST=localhost
DATABASE_PORT=3306
DATABASE_USERNAME=root
DATABASE_PASSWORD=root1234
DATABASE_NAME=dev_db

# CACHE
CACHE_HOST=localhost
CACHE_PORT=6379

# STRICT
IS_STRICT=true
```

### Result

```typescript
/* eslint-disable @typescript-eslint/no-non-null-assertion */
export const environment = {
  tz: process.env.TZ! as string,
  node: {
    env: process.env.NODE_ENV! as string
  },
  database: {
    host: process.env.DATABASE_HOST! as string,
    port: +process.env.DATABASE_PORT! as number,
    username: process.env.DATABASE_USERNAME! as string,
    password: process.env.DATABASE_PASSWORD! as string,
    name: process.env.DATABASE_NAME! as string
  },
  cache: {
    host: process.env.CACHE_HOST! as string,
    port: +process.env.CACHE_PORT! as number
  },
  is: {
    strict: (process.env.IS_STRICT! === 'true') as boolean
  }
}
```
