export const validateArgv = () => {
  const indexes = [process.argv.findIndex((each) => each === '-f'), process.argv.findIndex((each) => each === '-o')];
  if (indexes.includes(-1)) {
    throw new Error(`envFilePath or outFilePath is required. enter paths using -f and -o`);
  }

  return {
    envFilePath: process.argv[indexes[0] + 1],
    outFilePath: process.argv[indexes[1] + 1],
  };
};
