import * as fs from 'fs/promises';
import { convertValueType } from './convert-value-type';
import { generateConstFile } from './generate-const-file';
import { validateArgv } from './validate-argv';

const bootstrap = async () => {
  const { envFilePath, outFilePath } = validateArgv();

  const json = (await fs.readFile(envFilePath, 'utf8'))
    .split('\n')
    .filter((each) => each.includes('=') && !each.startsWith('#'))
    .reduce((acc, each) => {
      const [key, value] = each.split('=');
      const elements = key.split('_').map((each) => each.toLowerCase());

      elements.reduce((nestedAcc, element, index) => {
        if (index === elements.length - 1) {
          nestedAcc[element] = convertValueType(key, value);
        } else {
          nestedAcc[element] = nestedAcc[element] ?? {};
        }

        return nestedAcc[element];
      }, acc);

      return acc;
    }, {} as Record<string, any>);

  await generateConstFile({ path: outFilePath, json });
};

bootstrap();
