import * as fs from 'fs/promises';

export const generateConstFile = async (params: { path: string; json: Record<string, string | number | boolean> }) => {
  const { path, json } = params;

  await fs.rm(path).catch((err) => {
    if (err.code !== 'ENOENT') throw err;
  });

  await fs.writeFile(
    path,
    `
/* eslint-disable @typescript-eslint/no-non-null-assertion */
export const environment = ${JSON.stringify(json, null, 2).replace(/"/gi, '')}
`.trim(),
    'utf8',
  );
};
