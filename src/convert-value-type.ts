export const convertValueType = (key: string, value: string) => {
  const commentRemovedValue = value.split('#')[0].trim();

  if (commentRemovedValue === '') {
    throw Error(`invalid value exception. ${key}=${value}`);
  }

  if (commentRemovedValue === 'true' || commentRemovedValue === 'false') {
    return `(process.env.${key}! === 'true') as boolean`;
  }

  if (!isNaN(+commentRemovedValue)) {
    return `+process.env.${key}! as number`;
  }

  return `process.env.${key}! as string`;
};
